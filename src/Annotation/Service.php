<?php
namespace d84\Slim\Annotation;

use Doctrine\Common\Annotations\Annotation\Required;

/**
* @Annotation
* @Target({"CLASS","METHOD"})
*/
final class Service
{
    /**
     * @var string
     * @Required
     */
    public $name = '';
}
