<?php
namespace d84\Slim\Annotation;

use Doctrine\Common\Annotations\Annotation\Required;

/**
* @Annotation
* @Target({"CLASS","METHOD"})
*/
final class Factory
{
    /**
     * @var string
     * @Required
     */
    public $name = '';
}
