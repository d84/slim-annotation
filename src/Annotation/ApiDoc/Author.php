<?php
namespace d84\Slim\Annotation\ApiDoc;

/**
* @Annotation
* @Target({"METHOD","CLASS"})
*/
final class Author
{
    /**
     * @param array $data []
     *                      ['value'] array|string
     */
    public function __construct(array $data)
    {
        if (isset($data['value'])) {
            if (is_string($data['value'])) {
                $this->value = [$data['value']];
            } else {
                $this->value = $data['value'];
            }
        } else {
            throw new \RuntimeException("Not defined value");
        }
    }
}
