<?php
namespace d84\Slim\Annotation\ApiDoc;

use Doctrine\Common\Annotations\Annotation\Required;

/**
* @Annotation
* @Target({"METHOD","CLASS"})
*/
final class ApiDoc
{
    /**
     * @var array
     * @Required
     */
    public $author;

    /**
     * @var string
     */
    public $since = '';

    /**
     * @var string
     */
    public $version = '';

    /**
     * @var bool
     */
    public $deprecated = false;

    /**
     * @var string
     */
    public $description = '';

    /**
     * @return array
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @return string
     */
    public function getSince()
    {
        return $this->since;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function getDeprecated()
    {
        return $this->deprecated;
    }
}
