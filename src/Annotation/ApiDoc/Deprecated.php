<?php
namespace d84\Slim\Annotation\ApiDoc;

/**
* @Annotation
* @Target({"METHOD","CLASS"})
*/
final class Deprecated
{
}
