<?php
namespace d84\Slim\Annotation\ApiDoc;

/**
* @Annotation
* @Target({"METHOD","CLASS"})
*/
final class Since
{
    /**
     * @param array $data []
     *                      ['value'] string
     */
    public function __construct(array $data)
    {
        if (isset($data['value'])) {
            $this->value = $data['value'];
        } else {
            throw new \RuntimeException("Not defined value");
        }
    }
}
