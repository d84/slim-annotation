<?php
namespace d84\Slim\Annotation;

/**
 * WITHOUT ARGUMENTS
 *
 * @Middleware(callable="App\Middleware\ClientIp")
 * public function actionMethod()
 * {
 * }
 *
 * ....
 *
 * WITH ARGUMENTS
 *
 * @Middleware(
 *  callable="App\Middleware\ClientIp",
 *  arguments={
 *    true, null, {"argument_name" => "value"}
 *  }
 * )
 * public function actionMethod()
 * {
 * }
 */

/**
* @Annotation
* @Target({"CLASS","METHOD"})
*/
final class Middleware
{
    /**
     * @param array $data []
     *                      ['callable']  string
     *                      ['arguments'] array
     */
    public function __construct(array $data)
    {
        if (isset($data['value'])) {
            $this->callable = $data['value'];
        } else {
            throw new \RuntimeException("Not defined callable");
        }

        if (isset($data['arguments']) && !is_array($data['arguments'])) {
            throw new \RuntimeException("The value of 'arguments' must be an array");
        }

        $this->arguments = $data['arguments'] ?? null;
    }
}
