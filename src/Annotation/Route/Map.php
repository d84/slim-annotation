<?php
namespace d84\Slim\Annotation\Route;

/**
* @Annotation
* @Target({"METHOD"})
*/
final class Map
{
    /**
     * @param array $data []
     *                      ['name']    string
     *                      ['path']    string
     *                      ['method']  array
     */
    public function __construct(array $data)
    {
        $this->name = $data['name'] ?? '';
        $this->path = $data['path'] ?? null;
        if (is_null($this->path)) {
            throw new \RuntimeException("Not defined 'path' attribute");
        }
        $this->method = $data['method'] ?? null;
        if (is_null($this->method)) {
            throw new \RuntimeException("Not defined 'method' attribute");
        }
        if (!is_array($this->method)) {
            throw new \RuntimeException("The value of 'method' must be an array");
        }
        if (empty($this->method)) {
            throw new \RuntimeException("The value of 'method' must be a non empty array");
        }
        $enum = ['get', 'post', 'put', 'delete', 'patch', 'options'];
        foreach ($this->method as $method) {
            if (!in_array(strtolower($method), $enum)) {
                throw new \RuntimeException("Not allowed method '$method'. Allowed [" . implode(',', $enum) . "]");
            }
        }
    }
}
