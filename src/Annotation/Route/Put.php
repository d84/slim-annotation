<?php
namespace d84\Slim\Annotation\Route;

use Doctrine\Common\Annotations\Annotation\Required;
use Doctrine\Common\Annotations\Annotation\Enum;

/**
* @Annotation
* @Target({"METHOD"})
*/
final class Put
{
    /**
     * @var string
     */
    public $name = '';

    /**
     * @var string
     * @Required
     */
    public $path;

    /**
     * @var string
     * @Enum({"PUT"})
     */
    public $method = 'PUT';
}
