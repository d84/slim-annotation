<?php
namespace d84\Slim\Annotation\Route;

use Doctrine\Common\Annotations\Annotation\Enum;
use Doctrine\Common\Annotations\Annotation\Required;

/**
* @Annotation
* @Target({"METHOD"})
*/
final class Route
{
    /**
     * @var string
     */
    public $name = '';

    /**
     * @var string
     * @Required
     */
    public $path;

    /**
     * @var string
     * @Enum({"GET","POST","PUT","PATCH","DELETE","OPTIONS","ANY"})
     * @Required
     */
    public $method;
}
