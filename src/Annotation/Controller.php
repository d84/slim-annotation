<?php
namespace d84\Slim\Annotation;

/**
* @Annotation
* @Target({"CLASS"})
*/
final class Controller
{
    /**
     * @var string
     */
    public $path = '';
}
